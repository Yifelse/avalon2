package protocol

import (
	"bytes"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"reflect"
	"regexp"
	"strconv"
	"strings"

	//"bitbucket.org/chenxing/avalon/encoding"
	"bitbucket.org/jesgoo/avalon/encoding"
	"github.com/golang/protobuf/proto"
	// "github.com/yangchenxing/cangshan/application"
	"github.com/yangchenxing/foochow/logging"
)

// func init() {
// 	application.RegisterModulePrototype("EventEncoding", new(EventEncoding))
// }

var (
	queryPattern  = regexp.MustCompile("^(.*\\+)?[-0-9a-zA-Z_]+\\.[-0-9a-zA-Z_]+(\\.[0-9a-zA-Z]{10,130})?")
	extraDecoders = map[uint64]func(value string, event *Event) error{
		0: decodeTouchX,
		1: decodeTouchY,
		2: decodePressTime,
		3: decodeScrollNum,
		4: decodeScrollTime,
		5: decodeHeight,
		6: decodeWidth,
		7: decodeClickDelta,
		8: decodeOnMask,
		9: decodeDispatchTime,
	}
	extraEncoders = map[uint64]func(value interface{}) (string, error){
		1:  encodeUnknowns,
		2:  encodeTouchX,
		3:  encodeTouchY,
		4:  encodePressTime,
		5:  encodeScrollNum,
		6:  encodeScrollTime,
		7:  encodeWidth,
		8:  encodeHeight,
		9:  encodeClickDelta,
		10: encodeOnMask,
		11: encodeDispatchTime,
	}
	decodingErrorEventCache = make(map[Event_Head_DecodingError]*Event)
	protoTrue               = proto.Bool(true)
	protoFalse              = proto.Bool(false)
)

type RSACrypto interface {
	Decrypt(keyID string, input []byte) ([]byte, error)
	Encrypt(keyID string, input []byte) ([]byte, error)
}

type EventEncoding struct {
	RSACrypto    RSACrypto
	Debug        bool
	DefaultKeyID int
	IgnoreExtra  bool
}

func (enc EventEncoding) Decode(src string) (*Event, error) {
	logging.Debug("解码事件: %s", src)
	var err error
	event := new(Event)

	src = queryPattern.FindString(src)
	if src == "" {
		return enc.newDecodingErrorEvent(Event_Head_FORMAT_ERROR), errors.New("事件编码格式错误")
	}
	// if !queryPattern.MatchString(src) {
	// 	if pos := strings.LastIndex(src, "."); pos == -1 {
	// 		return enc.newDecodingErrorEvent(Event_Head_FORMAT_ERROR), errors.New("事件编码格式错误")
	// 	} else if src = src[:pos]; !queryPattern.MatchString(src) {
	// 		return enc.newDecodingErrorEvent(Event_Head_FORMAT_ERROR), errors.New("事件编码格式错误")
	// 	}
	// }
	var targetURLParam string
	if pos := strings.Index(src, "+"); pos > 0 {
		if targetURLParam, err = url.QueryUnescape(src[:pos]); err != nil {
			return enc.newDecodingErrorEvent(Event_Head_PREFIX_ERROR), fmt.Errorf("前置参数解转义出错: %s", err.Error())
		}
		src = src[pos+1:]
	}
	if pos := strings.Index(src, "&"); pos > 0 {
		src = src[:pos]
	}
	sections := strings.Split(src, ".")
	// 解码事件头
	var event_head []byte
	if event_head, err = encoding.DecodeStandardBase64(sections[0]); err != nil {
		return enc.newDecodingErrorEvent(Event_Head_HEAD_BASE64_ERROR), fmt.Errorf("事件头Base64解码出错: %s", err.Error())
	}
	event.Head = new(Event_Head)
	if err = proto.Unmarshal(event_head, event.Head); err != nil {
		return enc.newDecodingErrorEvent(Event_Head_HEAD_PROTOBUF_ERROR), fmt.Errorf("事件头Protobuf解码出错: %s", err.Error())
	}
	// 解码事件体
	var event_body []byte
	switch event.Head.GetCryptoType() {
	case Event_Head_JESGOO_BASE64:
		if event_body, err = encoding.DecodeJesgooBase64(sections[1], uint(event.Head.GetCryptoParam())); err != nil {
			return enc.newDecodingErrorEvent(Event_Head_BODY_BASE64_ERROR), fmt.Errorf("事件体Base64解码出错: %s", err.Error())
		}
		event.Body = new(Event_Body)
		if err = proto.Unmarshal(event_body, event.Body); err != nil {
			return enc.newDecodingErrorEvent(Event_Head_BODY_PROTOBUF_ERROR), fmt.Errorf("事件体Protobuf解码出错: %s", err.Error())
		}
	default:
		return enc.newDecodingErrorEvent(Event_Head_UNKNOWN_CRYPTO_TYPE), errors.New("未知事件头编码类型")
	}
	if err := enc.appendTargetURLParams(event, targetURLParam); err != nil {
		return enc.newDecodingErrorEvent(Event_Head_APPEND_TARGET_PARAM_ERROR), err
	}
	// 解码补充字段，解码器未完全初始化前不解码补充字段
	if enc.RSACrypto == nil {
		if !enc.IgnoreExtra {
			logging.Warn("缺少RSACrypto，无法解码补充字段")
		}
	} else if len(sections) == 3 {
		var extra string
		if extra, err = enc.decryptExtra(sections[2]); err != nil {
			logging.Warn("解码事件补充字段出错: query=`%s`, error=`%s`", src, err.Error())
			enc.appendDecodeError(event, Event_Head_EXTRA_RSA_ERROR)
		} else {
			event.Extra = &Event_Extra{
				Unknown: make([]*Event_Extra_Item, 0, 1),
			}
			for _, field := range strings.Split(extra, ",") {
				colonPos := strings.Index(field, ":")
				if colonPos < 1 {
					logging.Warn("事件补充字段格式错误: query=`%s`, extra=`%s`", src, extra)
					enc.appendDecodeError(event, Event_Head_EXTRA_FORMAT_ERROR)
					break
				}
				var id uint64
				if id, err = strconv.ParseUint(field[:colonPos], 10, 32); err != nil {
					logging.Warn("事件补充字段格式错误: query=`%s`, extra=`%s`, error=`%s`",
						src, extra, err.Error())
					break
				}
				value := field[colonPos+1:]
				if decoder := extraDecoders[id]; decoder != nil {
					if err := decoder(value, event); err != nil {
						logging.Warn("事件补充字段解码出错: query=`%s`, extra=`%s`, error=`%s`",
							src, extra, err.Error())
						enc.appendDecodeError(event, Event_Head_EXTRA_FORMAT_ERROR)
						break
					}
				} else {
					event.Extra.Unknown = append(event.Extra.Unknown, &Event_Extra_Item{
						Id:    proto.Uint32(uint32(id)),
						Value: proto.String(value),
					})
				}
			}
		}
	} else {
		logging.Warn("点击串结构有问题，section数目为: %d", len(sections))
	}
	event.Head.DecodeSuccess = protoTrue
	return event, nil
}

func (enc EventEncoding) newDecodingErrorEvent(errorType Event_Head_DecodingError) *Event {
	event := decodingErrorEventCache[errorType]
	if event == nil {
		event = &Event{
			Head: &Event_Head{
				CryptoType:    Event_Head_JESGOO_BASE64.Enum(),
				CryptoParam:   proto.Uint32(0),
				DecodeSuccess: protoFalse,
				DecodeErrors:  []Event_Head_DecodingError{errorType},
			},
			Body: &Event_Body{
				Type: EventType_CLICK.Enum(),
			},
		}
		decodingErrorEventCache[errorType] = event
	}
	return event
}

func (enc EventEncoding) appendDecodeError(event *Event, errorType Event_Head_DecodingError) {
	if event.Head.DecodeErrors == nil {
		event.Head.DecodeErrors = []Event_Head_DecodingError{errorType}
	} else {
		event.Head.DecodeErrors = append(event.Head.DecodeErrors, errorType)
	}
}

func (enc EventEncoding) appendTargetURLParams(event *Event, targetURLParams string) error {
	if targetURLParams == "" {
		return nil
	}
	if event.Body.Ad == nil {
		return errors.New("Event.Body缺少Ad字段")
	}
	if interaction := event.Body.Ad.GetInteraction(); interaction != InteractionType_INTER_SURFING && interaction != InteractionType_INTER_DOWNLOAD {
		return nil
	}
	if event.Body.Action == nil {
		return errors.New("Event.Body缺少Action字段")
	}
	if event.Body.Action.TargetUrl == nil {
		return errors.New("Event.Body.Action缺少Target字段")
	}
	*event.Body.Action.TargetUrl += targetURLParams
	return nil
}

func (enc EventEncoding) Encode(event *Event) (string, error) {
	if enc.Debug {
		eventString, _ := json.Marshal(event)
		logging.Debug("编码事件: %s", eventString)
	}
	if event.Head == nil || event.Body == nil {
		return "", errors.New("缺少事件头或事件体")
	}
	var head string
	if headContent, err := proto.Marshal(event.GetHead()); err != nil {
		return "", fmt.Errorf("编码事件头Protobuf出错: %s", err.Error())
	} else {
		head = encoding.EncodeStandardBase64(headContent)
	}
	var body string
	if bodyContent, err := proto.Marshal(event.GetBody()); err != nil {
		return "", fmt.Errorf("编码事件体Protobuf出错: %s", err.Error())
	} else {
		switch event.Head.GetCryptoType() {
		case Event_Head_JESGOO_BASE64:
			body = encoding.EncodeJesgooBase64(bodyContent, uint(event.Head.GetCryptoParam()))
		default:
			return "", fmt.Errorf("未知事件体编码类型: %d", event.Head.GetCryptoType())
		}
	}
	if event.GetExtra() != nil && enc.RSACrypto != nil {
		var extraContent bytes.Buffer
		extraType := reflect.TypeOf(event.GetExtra()).Elem()
		extraValue := reflect.ValueOf(event.GetExtra()).Elem()
		for i := 0; i < extraType.NumField(); i += 1 {
			fieldValue := extraValue.Field(i)
			if fieldValue.IsNil() {
				continue
			}
			protobufTag := extraType.Field(i).Tag.Get("protobuf")
			if protobufTag == "" {
				continue
			} else if id, err := strconv.Atoi(strings.Split(protobufTag, ",")[1]); err != nil {
				return "", fmt.Errorf("protobuf标签字段ID解码出错: %s", err.Error())
			} else if encoder := extraEncoders[uint64(id)]; encoder == nil {
				continue
			} else if fieldContent, err := encoder(fieldValue.Interface()); err != nil {
				return "", fmt.Errorf("编码Extra字段%s出错: %s", extraType.Field(i).Name, err.Error())
			} else if extraContent.Len() == 0 {
				extraContent.WriteString(fieldContent)
			} else {
				extraContent.WriteString("," + fieldContent)
			}
		}
		if extraContent.Len() > 0 {
			if extra, err := enc.encryptExtra(extraContent.Bytes()); err != nil {
				return "", fmt.Errorf("编码事件补充字段出错: %s", err.Error())
			} else {
				return fmt.Sprintf("%s.%s.%s", head, body, extra), nil
			}
		} else {
			return fmt.Sprintf("%s.%s", head, body), nil
		}
	} else {
		return fmt.Sprintf("%s.%s", head, body), nil
	}
}

type clickStruct struct {
	Down_x int
	Down_y int
	Slot_h int
	Slot_w int
}

func (enc EventEncoding) decryptExtra(data string) (string, error) {
	if len(data) <= 2 {
		return "", fmt.Errorf("Extra字段太短: %s", data)
	}
	ttype := data[:2]
	if ttype == "02" {
		if cipherText, err := hex.DecodeString(data[2:]); err != nil {
			return "", fmt.Errorf("十六进制解码失败: %s", err.Error())
		} else if plainText, err := enc.RSACrypto.Decrypt("event."+data[:2], cipherText); err == nil {
			return string(plainText), nil
		} else {
			return "", fmt.Errorf("RSA解码出错: %s", err.Error())
		}
	}
	// 标准base64 编码的点击坐标
	// {"down_x":123,"down_y":123,"up_x":123,"up_y":123}
	if ttype == "05" {
		posbyte, err := encoding.DecodeStandardBase64(data[2:])
		if err != nil {
			return "", fmt.Errorf("点击坐标base64解码失败: %s", err.Error())
		}
		var clickdata clickStruct
		err = json.Unmarshal(posbyte, &clickdata)
		if err != nil {
			return "", fmt.Errorf("反序列化点击坐标失败: %s", err.Error())
		}
		retdata := fmt.Sprintf("0:%d,1:%d,5:%d,6:%d", clickdata.Down_x, clickdata.Down_y, clickdata.Slot_h, clickdata.Slot_w)
		return retdata, nil
	}
	return "", fmt.Errorf("未知的extra类型: %s", ttype)
}

func (enc EventEncoding) encryptExtra(content []byte) (string, error) {
	if result, err := enc.RSACrypto.Encrypt(fmt.Sprintf("event.%02x", enc.DefaultKeyID), content); err != nil {
		return "", fmt.Errorf("RSA编码出错: %s", err.Error())
	} else {
		return fmt.Sprintf("%02x%s", enc.DefaultKeyID, hex.EncodeToString(result)), nil
	}
}

func encodeUnknowns(value interface{}) (string, error) {
	var buf bytes.Buffer
	for _, item := range value.([]*Event_Extra_Item) {
		if buf.Len() == 0 {
			fmt.Fprintf(&buf, "%d:%s", item.GetId(), item.GetValue())
		} else {
			fmt.Fprintf(&buf, ",%d:%s", item.GetId(), item.GetValue())
		}
	}
	return buf.String(), nil
}

func decodeTouchX(value string, event *Event) error {
	if v, err := strconv.Atoi(value); err != nil {
		return fmt.Errorf("TouchX格式错误: value=%q, error=%q", value, err.Error())
	} else {
		event.Extra.TouchX = proto.Uint32(uint32(v))
	}
	return nil
}

func encodeTouchX(value interface{}) (string, error) {
	return fmt.Sprintf("0:%d", *(value.(*uint32))), nil
}

func decodeTouchY(value string, event *Event) error {
	if v, err := strconv.Atoi(value); err != nil {
		return fmt.Errorf("TouchY格式错误: value=%q, error=%q", value, err.Error())
	} else {
		event.Extra.TouchY = proto.Uint32(uint32(v))
	}
	return nil
}

func encodeTouchY(value interface{}) (string, error) {
	return fmt.Sprintf("1:%d", *(value.(*uint32))), nil
}

func decodePressTime(value string, event *Event) error {
	if v, err := strconv.Atoi(value); err != nil {
		return fmt.Errorf("PressTime格式错误: value=%q, error=%q", value, err.Error())
	} else {
		event.Extra.PressTime = proto.Uint32(uint32(v))
	}
	return nil
}

func encodePressTime(value interface{}) (string, error) {
	return fmt.Sprintf("2:%d", *(value.(*uint32))), nil
}

func decodeScrollNum(value string, event *Event) error {
	if v, err := strconv.Atoi(value); err != nil {
		return fmt.Errorf("ScrollNum格式错误: value=%q, error=%q", value, err.Error())
	} else {
		event.Extra.ScrollNum = proto.Uint32(uint32(v))
	}
	return nil
}

func encodeScrollNum(value interface{}) (string, error) {
	return fmt.Sprintf("3:%d", *(value.(*uint32))), nil
}

func decodeScrollTime(value string, event *Event) error {
	if v, err := strconv.Atoi(value); err != nil {
		return fmt.Errorf("ScrollTime格式错误: value=%q, error=%q", value, err.Error())
	} else {
		event.Extra.ScrollTime = proto.Uint32(uint32(v))
	}
	return nil
}

func encodeScrollTime(value interface{}) (string, error) {
	return fmt.Sprintf("4:%d", *(value.(*uint32))), nil
}

func decodeWidth(value string, event *Event) error {
	if v, err := strconv.Atoi(value); err != nil {
		return fmt.Errorf("Width格式错误: value=%q, error=%q", value, err.Error())
	} else {
		event.Extra.Width = proto.Uint32(uint32(v))
	}
	return nil
}

func encodeWidth(value interface{}) (string, error) {
	return fmt.Sprintf("6:%d", *(value.(*uint32))), nil
}

func decodeHeight(value string, event *Event) error {
	if v, err := strconv.Atoi(value); err != nil {
		return fmt.Errorf("Height格式错误: value=%q, error=%q", value, err.Error())
	} else {
		event.Extra.Height = proto.Uint32(uint32(v))
	}
	return nil
}

func encodeHeight(value interface{}) (string, error) {
	return fmt.Sprintf("5:%d", *(value.(*uint32))), nil
}

func decodeClickDelta(value string, event *Event) error {
	if v, err := strconv.ParseFloat(value, 64); err != nil {
		return fmt.Errorf("ClickDelta格式错误: value=%q, error=%q", value, err.Error())
	} else {
		event.Extra.ClickDelta = proto.Float64(v / 10)
	}
	return nil
}

func encodeClickDelta(value interface{}) (string, error) {
	return fmt.Sprintf("7:%.1f", *(value.(*float64))), nil
}

func decodeOnMask(value string, event *Event) error {
	if value == "1" {
		event.Extra.OnMask = proto.Bool(true)
	} else {
		event.Extra.OnMask = proto.Bool(false)
	}
	return nil
}

func encodeOnMask(value interface{}) (string, error) {
	if b, ok := value.(bool); ok && b {
		return "1", nil
	} else {
		return "0", nil
	}
}

func decodeDispatchTime(value string, event *Event) error {
	if v, err := strconv.Atoi(value); err != nil {
		return fmt.Errorf("DispatchTime格式错误: value=%q, error=%q", value, err.Error())
	} else {
		event.Extra.DispatchTime = proto.Uint32(uint32(v))
	}
	return nil
}

func encodeDispatchTime(value interface{}) (string, error) {
	return fmt.Sprintf("9:%d", *(value.(*uint32))), nil
}
