package protocol

import (
	"bytes"
	"encoding/binary"
	"net"
	"time"

	"github.com/golang/protobuf/proto"
)

var (
	timeLocation = time.Now().Location()
)

func (m *Event_Body) GetSearchIP() net.IP {
	if m != nil && m.SearchIp != nil {
		return convertIntToIP(*m.SearchIp)
	}
	return nil
}

func (m *Event_Body) GetEventIP() net.IP {
	if m != nil && m.EventIp != nil {
		return convertIntToIP(*m.EventIp)
	}
	return nil
}

func (m *Event_Body) SetSearchIP(ip net.IP) {
	if m != nil && ip != nil {
		m.SearchIp = proto.Uint32(convertIPToInt(ip))
	}
}

func (m *Event_Body) SetEventIP(ip net.IP) {
	if m != nil && ip != nil {
		m.EventIp = proto.Uint32(convertIPToInt(ip))
	}
}

func (m *Event_Body) GetSearchTime() time.Time {
	return time.Unix(int64(m.GetSearchTimestamp()), 0).In(timeLocation)
}

func (m *Event_Body) GetEventTime() time.Time {
	return time.Unix(int64(m.GetEventTimestamp()), 0).In(timeLocation)
}

func convertIntToIP(val uint32) net.IP {
	var buf bytes.Buffer
	binary.Write(&buf, binary.BigEndian, val)
	return net.IP(buf.Bytes())
}

func convertIPToInt(ip net.IP) uint32 {
	var val uint32
	binary.Read(bytes.NewBuffer(ip.To4()), binary.BigEndian, &val)
	return val
}
