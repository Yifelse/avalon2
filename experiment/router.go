package experiment

import (
	"fmt"
	"hash/crc32"
	"math/rand"
	"reflect"

	"github.com/yangchenxing/foochow/logging"
	"github.com/yangchenxing/foochow/structs"
)

func init() {
	routerFactory := structs.NewGeneralInterfaceFactory(reflect.TypeOf((*Router)(nil)).Elem(), "Type",
		func(instance interface{}) error {
			return instance.(Router).Initialize()
		})
	structs.RegisterFactory(routerFactory)
	routerFactory.RegisterType("Random", reflect.TypeOf(new(RandRouter)).Elem())
	routerFactory.RegisterType("HashRandom", reflect.TypeOf(new(HashRandRouter)).Elem())
	routerFactory.RegisterType("EqualInt", reflect.TypeOf(new(EqualIntRouter)).Elem())
	routerFactory.RegisterType("EqualString", reflect.TypeOf(new(EqualStringRouter)).Elem())
}

type Router interface {
	Initialize() error
	Route(Features, logging.Logger) int
}

// 完全随机选择
type RandRouter struct {
	Choices []uint
}

func (router *RandRouter) Initialize() error {
	return nil
}

func (router *RandRouter) Route(_ Features, logger logging.Logger) int {
	value := uint(rand.Intn(1000) + 1)
	for i, percent := range router.Choices {
		if value <= percent {
			log(logger, "debug", "RandRouter选择完成: choice=%d", i)
			return i
		}
		value -= percent
	}
	return -1
}

// 字符串散列随机
type HashRandRouter struct {
	FeatureName string
	Choices     []uint
}

func (router *HashRandRouter) Initialize() error {
	return nil
}

func (router *HashRandRouter) Route(features Features, logger logging.Logger) int {
	if text, ok := features.GetFeature(router.FeatureName).(string); ok {
		value := uint(crc32.ChecksumIEEE([]byte(text))%1000 + 1)
		for i, percent := range router.Choices {
			if value <= percent {
				log(logger, "debug", "HashRandRouter选择完成: choice=%d", i)
				return i
			}
			value -= percent
		}
	}
	log(logger, "warn", "HashRandRouter选择出错: feature=%q, error=\"特征不存在或类型不是string\"", router.FeatureName)
	return -1
}

// 整数相等
type EqualIntRouter struct {
	FeatureNames []string
	Values       [][]int64

	values []map[int64]bool
}

func (router *EqualIntRouter) Initialize() error {
	router.values = make([]map[int64]bool, len(router.Values))
	for i, values := range router.Values {
		router.values[i] = make(map[int64]bool)
		for _, value := range values {
			router.values[i][value] = true
		}
	}
	return nil
}

func (router *EqualIntRouter) Route(features Features, logger logging.Logger) int {
	for _, featureName := range router.FeatureNames {
		if value, err := router.normalizeValue(features.GetFeature(featureName)); err != nil {
			log(logger, "warn", "EqualIntRouter选择出错: feature=%q, error=\"特征不存在或非整数\"")
		} else {
			for i, values := range router.values {
				if values[value] {
					log(logger, "debug", "EqualIntRouter选择完成: choice=%d, feature=%q, value=%d", i, featureName, value)
					return i
				}
			}
		}
	}
	log(logger, "debug", "EqualIntRouter选择完成: choice=%d", len(router.values))
	return len(router.values)
}

func (router *EqualIntRouter) normalizeValue(value interface{}) (int64, error) {
	switch value.(type) {
	case int:
		return int64(value.(int)), nil
	case int16:
		return int64(value.(int16)), nil
	case int32:
		return int64(value.(int32)), nil
	case int64:
		return value.(int64), nil
	case uint:
		return int64(value.(uint)), nil
	case uint16:
		return int64(value.(uint16)), nil
	case uint32:
		return int64(value.(uint32)), nil
	case uint64:
		return int64(value.(uint64)), nil
	}
	return 0, fmt.Errorf("EqualIntRouter不支持的特征类型: %s", reflect.TypeOf(value).Name())
}

// 字符串相等
type EqualStringRouter struct {
	FeatureNames []string
	Values       [][]string

	values []map[string]bool
}

func (router *EqualStringRouter) Initialize() error {
	router.values = make([]map[string]bool, len(router.Values))
	for i, values := range router.Values {
		router.values[i] = make(map[string]bool)
		for _, value := range values {
			router.values[i][value] = true
		}
	}
	return nil
}

func (router *EqualStringRouter) Route(features Features, logger logging.Logger) int {
	for _, featureName := range router.FeatureNames {
		if value, ok := features.GetFeature(featureName).(string); !ok {
			log(logger, "warn", "EqualStringRouter选择出错: feature=%q, error=\"特征不存在或非字符串\"", featureName)
		} else {
			for i, values := range router.values {
				if values[value] {
					log(logger, "debug", "EqualStringRouter选择完成: choice=%d, feature=%q, value=%s", i, featureName, value)
					return i
				}
			}
		}
	}
	log(logger, "debug", "EqualStringRouter选择完成: choice=%d", len(router.values))
	return len(router.values)
}
