package experiment

import "github.com/yangchenxing/foochow/logging"

type Features interface {
	SetFeature(name string, value interface{})
	GetFeature(name string) interface{}
}

type BranchMark struct {
	Name  string
	Value interface{}
}

type Branch struct {
	Name   string
	Router Router
	Marks  []struct {
		Name  string
		Value interface{}
	}
	Children []*Branch
}

func (branch *Branch) Decide(features Features, logger logging.Logger) {
	logging.Debug("进入实验节点: %s", branch.Name)
	for _, mark := range branch.Marks {
		features.SetFeature(mark.Name, mark.Value)
	}
	if branch.Router != nil {
		if choice := branch.Router.Route(features, logger); choice < 0 {
			log(logger, "debug", "Router暂停分支: branch=%q, choice=%d", branch.Name, choice)
		} else if choice >= len(branch.Children) {
			log(logger, "debug", "Router暂停分支: branch=%q, choice=%d", branch.Name, choice)
		} else {
			branch.Children[choice].Decide(features, logger)
		}
	} else {
		for _, branch := range branch.Children {
			branch.Decide(features, logger)
		}
	}
}

func log(logger logging.Logger, level, pattern string, args ...interface{}) {
	if logger != nil {
		logger(2, level, pattern, args...)
	}
}
