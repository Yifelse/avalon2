package encoding

import "encoding/json"

func Jsonify(i interface{}) string {
	s, _ := json.Marshal(i)
	return string(s)
}
