package encoding

import (
	"bytes"
	"crypto/md5"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"net"
	"time"
)

func DecodeJesgooID(jesgooId string) (ip net.IP, timestamp time.Time, err error) {
	if len(jesgooId) != 32 {
		err = errors.New("JesgooID格式错误")
	} else if buffer, err := hex.DecodeString(jesgooId); err != nil {
		err = errors.New("JesgooID格式错误")
	} else {
		var expectCheckSumValue uint32
		var actualCheckSumValue uint32
		var ipValue uint32
		var timestampValue uint32
		var randomValue uint32
		bufferReader := bytes.NewReader(buffer)
		binary.Read(bufferReader, binary.BigEndian, &expectCheckSumValue)
		binary.Read(bufferReader, binary.BigEndian, &ipValue)
		binary.Read(bufferReader, binary.BigEndian, &timestampValue)
		binary.Read(bufferReader, binary.BigEndian, &randomValue)
		checkSumBuffer := new(bytes.Buffer)
		binary.Write(checkSumBuffer, binary.LittleEndian, ipValue)
		binary.Write(checkSumBuffer, binary.BigEndian, timestampValue)
		binary.Write(checkSumBuffer, binary.BigEndian, randomValue)
		checkSumBuffer.WriteString("JESGOOID_SECRET_PADDING")
		checkSumBytes := md5.Sum(checkSumBuffer.Bytes())
		binary.Read(bytes.NewReader(checkSumBytes[11:15]), binary.LittleEndian, &actualCheckSumValue)
		ip = net.IPv4(buffer[7], buffer[6], buffer[5], buffer[4])
		timestamp = ParseTimestamp(int64(timestampValue))
		if expectCheckSumValue != actualCheckSumValue {
			err = errors.New("JesgooID校验错误")
		}
	}
	return
}
