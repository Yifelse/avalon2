package encoding

import (
	"time"
)

// var (
// 	location        = time.Now().Location()
// )

func init() {
}

func ParseTimestamp(timestamp int64) time.Time {
	return time.Unix(timestamp, 0)
}

// func FormatUTCTimestamp(timestamp time.Time) int64 {
// 	return timestamp.UTC().Unix()
// }
//
// func ParseUTCTimestamp(timestamp int64) time.Time {
// 	return time.Unix(timestamp, 0).In(location)
// }
