package profstats

import (
	"fmt"
	"os"
	"runtime"

	"github.com/shirou/gopsutil/process"
	"github.com/yangchenxing/foochow/influxstats"
	"github.com/yangchenxing/foochow/logging"
)

var (
	lastNumGC        uint32
	lastPauseTotalNs uint64
	proc             *process.Process
	host, _          = os.Hostname()

	MeasurementPerformance = "performance"
	PerformanceTag         = stats.NewTags("host", host)
)

func init() {
	var err error
	proc, err = process.NewProcess(int32(os.Getpid()))
	if err != nil {
		fmt.Fprintf(os.Stderr, "获取当前进程出错: pid=%d, error=%q", os.Getpid(), err.Error())
		os.Exit(1)
	}
	//proc.CPUPercent(0)
	proc.Percent(0)
}

func StatsSysPerformance(items stats.ItemSet) {
	items.SetInt(MeasurementPerformance, PerformanceTag, "CPU.Count", int64(runtime.NumCPU()))
	items.SetInt(MeasurementPerformance, PerformanceTag, "Goroutine.Count", int64(runtime.NumGoroutine()))

	var memStats runtime.MemStats

	runtime.ReadMemStats(&memStats)

	items.SetInt(MeasurementPerformance, PerformanceTag, "Memory.Alloc", int64(memStats.Alloc))
	items.SetInt(MeasurementPerformance, PerformanceTag, "Memory.TotalAlloc", int64(memStats.TotalAlloc))
	items.SetInt(MeasurementPerformance, PerformanceTag, "Memory.SysAlloc", int64(memStats.Sys))
	items.SetInt(MeasurementPerformance, PerformanceTag, "Memory.HeapObjects", int64(memStats.HeapObjects))

	items.SetInt(MeasurementPerformance, PerformanceTag, "Memory.GCCount", int64(memStats.NumGC-lastNumGC))
	lastNumGC = memStats.NumGC
	items.SetInt(MeasurementPerformance, PerformanceTag, "Memory.GCPauseNs", int64(memStats.PauseTotalNs-lastPauseTotalNs))
	lastPauseTotalNs = memStats.PauseTotalNs

	//if cpuPercent, err := proc.CPUPercent(0); err != nil {
	if cpuPercent, err := proc.Percent(0); err != nil {
		logging.Error("获取CPU占用率出错: %s", err.Error())
	} else {
		items.SetFloat(MeasurementPerformance, PerformanceTag, "CPU.Percent", cpuPercent)
	}
	if numfds, err := proc.NumFDs(); err != nil {
		logging.Error("获取进程FD数量出错: %s", err.Error())
	} else {
		items.SetInt(MeasurementPerformance, PerformanceTag, "numfds", int64(numfds))
	}
	if numthreads, err := proc.NumThreads(); err != nil {
		logging.Error("获取线程数出错: %s", err.Error())
	} else {
		items.SetInt(MeasurementPerformance, PerformanceTag, "numthreads", int64(numthreads))
	}
}
